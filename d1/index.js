/*
	Placeholder API https://jsonplaceholder.typicode.com/posts
*/

// get Data
fetch('https://jsonplaceholder.typicode.com/posts').then((response) => response.json()).then((data) => showPosts(data));



// show Posts
/*
	will receive the posts as parameters
		create a postEntries with an empty string as its value

		in the posts parameter, perform forEach method and render the following HTML elements as the new value for the post entries
			- create a div with id of "post-post.id"
				- create h3 element with the id of "post-title-post.id" with the value of post.title
				- create a p element with the id of "post-body-post.id" with the value of post.body
				- create 2 buttons
					- edit button with onclick event handler calling editPosts function receiving post.id as its argument
					- delete button with onclick event handler calling deletePosts function receiving post.id as its argument
*/

const showPosts = (posts) => {
	let postEntries = '';

	posts.forEach((post)=>{
	postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost(${post.id})">Edit</button>
				<button onclick="deletePost(${post.id})">Delete</button>
			</div>
		`
	});
	document.querySelector('#div-post-entries').innerHTML = postEntries;
};


// add Post Data

document.querySelector('#form-add-post').addEventListener('submit', (e) => {
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts',{
		method:"POST",
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		}),
		headers: {'Content-type':'application/json; charset=UTF-8'}
	})
.then((response) => response.json())
.then((data) =>{
	console.log(data);
	alert('Successfully added');

	// lets input field be cleared once alert is closed
	document.querySelector('#txt-title').value = null;
	document.querySelector('#txt-body').value = null;
	})
});



// Edit Post
const editPost = (id) =>{
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
	document.querySelector('#btn-submit-update').removeAttribute('disabled');

};

// Update Post
/*
	for function, select the edit post form through the use of document.querySelector, it should listen to submit event and will perform the following block of codes
		- set the method to PUT
		- send the body as stringified json with the following properties:
			- id = id input field of the edit post form
			- title - title input field of the edit post form
			- body - input field of the edit post form
			- userId = 1

		- set the headers for content-type of application/json, charset=UTF-8
		- use then methods to convert the response into json and log it in the console
		- send an alert for successful updating
		- empty the edit post form after the codes has been executed
*/

document.querySelector('#form-edit-post').addEventListener('submit', (e) =>{
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: "PUT",
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		}),
		headers: {'Content-type':'application/json; charset=UTF-8'}
	})
	.then((response) => response.json())
	.then((data) =>{
		console.log(data);
		alert('Successfully added');

		// lets the updated object to be displayed in the frontend
		document.querySelector(`#post-title-${data.id}`).innerHTML = data.title;
		document.querySelector(`#post-body-${data.id}`).innerHTML = data.body;


		// lets input field be cleared once alert is closed
		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;

		// setAttribute is used to allow setting up or bring back the removed attribute of the HTML elements
		/*
			accepts 2 arguments
				1st - the string that identifies the attribute
				2nd - boolean to set if attribute is true or false
		*/
		document.querySelector('#btn-submit-update').setAttribute('disabled', true);
	});
});

// Activity

const deletePost = (id) => {
    document.querySelector(`#post-${id}`).remove();
    alert('Successfully deleted');
}
